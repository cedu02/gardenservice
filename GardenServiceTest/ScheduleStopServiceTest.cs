using System;
using FluentAssertions;
using GardenCore.Enums;
using GardenCore.Models;
using GardenService.Services.Schedule;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Xunit;

namespace GardenServiceTest {
    public class ScheduleStopServiceTest {
        [Fact]
        public void CheckIfShouldBeStopped_TimeOfScheduleIsOver_ReturnsTrue() {
            // Arrange 
            var sut = CreateServiceUnderTest();
            const float duration = (float) 1.2;
            var lastTask = new GardenTaskModel {
                ArduinoTask = ArduinoTaskEnum.StartFan,
                Timestamp = DateTime.Now.AddHours(-(duration - 0.1))
            };
            var schedule = new ScheduleModel {
                ScheduleType = ScheduleTypeEnum.SwitchFan,
                Duration = duration
            };
            const bool isRunning = true;

            // Act
            var actual = sut.CheckIfShouldBeStopped(schedule, lastTask, isRunning);
            // Assert
            actual.Should().BeTrue();
        }


        [Fact]
        public void CheckIfShouldBeStopped_IsNotOver_ReturnsFalse() {
            // Arrange 
            var sut = CreateServiceUnderTest();
            const float duration = (float) 1.2;
            var lastTask = new GardenTaskModel {
                ArduinoTask = ArduinoTaskEnum.StartFan,
                Timestamp = DateTime.Now.AddHours(-(2 * duration))
            };
            var schedule = new ScheduleModel {
                ScheduleType = ScheduleTypeEnum.SwitchFan,
                Duration = duration
            };
            const bool isRunning = true;

            // Act
            var actual = sut.CheckIfShouldBeStopped(schedule, lastTask, isRunning);
            // Assert
            actual.Should().BeFalse();
        }


        [Fact]
        public void CheckIfShouldBeStopped_IsRunningButShouldNot_ReturnsFalse() {
            // Arrange 
            var loggerMock = new Mock<ILogger<ScheduleStopService>>();

            var sut = new ScheduleStopService(loggerMock.Object);
            const float duration = (float) 1.2;
            var lastTask = new GardenTaskModel {
                ArduinoTask = ArduinoTaskEnum.StopFan,
                Timestamp = DateTime.Now.AddHours(-(2 * duration))
            };
            var schedule = new ScheduleModel {
                ScheduleType = ScheduleTypeEnum.SwitchFan,
                Duration = duration
            };
            const bool isRunning = true;

            // Act
            var actual = sut.CheckIfShouldBeStopped(schedule, lastTask, isRunning);
            // Assert
            actual.Should().BeFalse();
            loggerMock.Verify(l => l.Log(
                LogLevel.Warning,
                It.IsAny<EventId>(),
                It.IsAny<It.IsAnyType>(),
                It.IsAny<Exception>(),
                (Func<It.IsAnyType, Exception, string>)
                It.IsAny<object>()), Times.Exactly(1));
        }

        private static ScheduleStopService CreateServiceUnderTest() {
            return new(NullLogger<ScheduleStopService>.Instance);
        }
    }
}