using System.Threading.Tasks;
using FluentAssertions;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using GardenCore.Models;
using GardenService.Interfaces;
using GardenService.Services.Schedule;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Xunit;

namespace GardenServiceTest {
    public class ScheduleServiceTest {
        [Fact]
        public async Task CheckSchedules_InvalidSchedule_ReturnsFalse() {
            // Arrange 
            var mockRepo = new Mock<IScheduleRepository>();
            mockRepo.Setup(m =>
                    m.GetSchedule(It.IsAny<ScheduleTypeEnum>()))
                .ReturnsAsync(() => new ScheduleModel());
            var sut = CreateServiceUnderTest(mockRepo.Object,
                new Mock<IScheduleStartService>().Object,
                new Mock<IScheduleStopService>().Object,
                new Mock<IScheduleArduinoService>().Object);

            // Act 
            var actual = await sut.CheckSchedules();
            // Assert 
            actual.Should().BeFalse();
        }


        [Fact]
        public async Task CheckSchedules_StartsSchedule_ReturnsTrue() {
            // Arrange 
            var mockRepo = new Mock<IScheduleRepository>();
            mockRepo.Setup(m =>
                    m.GetSchedule(It.IsAny<ScheduleTypeEnum>()))
                .ReturnsAsync(() => new ScheduleModel {
                    Id = 1
                });

            var mockArduinoService = new Mock<IScheduleArduinoService>();
            mockArduinoService.Setup(a => a.GetLatestGardenTask(
                    It.IsAny<ScheduleModel>()))
                .ReturnsAsync(() => new GardenTaskModel());

            mockArduinoService.Setup(a =>
                    a.CheckIfScheduleIsRunning(It.IsAny<ScheduleModel>()))
                .ReturnsAsync(() => new RunningModel());

            mockArduinoService
                .Setup(a => a.ExecuteScheduleStartAction(It.IsAny<ScheduleModel>()))
                .ReturnsAsync(() => new TaskResult {
                    Success = true
                });

            var mockStartService = new Mock<IScheduleStartService>();
            mockStartService.Setup(s =>
                    s.CheckIfShouldBeStarted(It.IsAny<ScheduleModel>(),
                        It.IsAny<GardenTaskModel>(),
                        It.IsAny<bool>()))
                .Returns(() => true);

            var sut = CreateServiceUnderTest(mockRepo.Object,
                mockStartService.Object,
                new Mock<IScheduleStopService>().Object,
                mockArduinoService.Object);

            // Act 
            var actual = await sut.CheckSchedules();
            // Assert 
            actual.Should().BeTrue();
            mockRepo.Verify();
            mockArduinoService.Verify();
            mockStartService.Verify();
        }


        [Fact]
        public async Task CheckSchedules_StopsSchedule_ReturnsTrue() {
            // Arrange 
            var mockRepo = new Mock<IScheduleRepository>();
            mockRepo.Setup(m =>
                    m.GetSchedule(It.IsAny<ScheduleTypeEnum>()))
                .ReturnsAsync(() => new ScheduleModel {
                    Id = 1
                });

            var mockArduinoService = new Mock<IScheduleArduinoService>();
            mockArduinoService.Setup(a => a.GetLatestGardenTask(
                    It.IsAny<ScheduleModel>()))
                .ReturnsAsync(() => new GardenTaskModel());

            mockArduinoService.Setup(a =>
                    a.CheckIfScheduleIsRunning(It.IsAny<ScheduleModel>()))
                .ReturnsAsync(() => new RunningModel());

            mockArduinoService
                .Setup(a => a.ExecuteScheduleStopAction(It.IsAny<ScheduleModel>()))
                .ReturnsAsync(() => new TaskResult {
                    Success = true
                });

            var mockStartService = new Mock<IScheduleStartService>();
            mockStartService.Setup(s =>
                    s.CheckIfShouldBeStarted(It.IsAny<ScheduleModel>(),
                        It.IsAny<GardenTaskModel>(),
                        It.IsAny<bool>()))
                .Returns(() => false);


            var mockStopService = new Mock<IScheduleStopService>();
            mockStopService.Setup(m => m.CheckIfShouldBeStopped(
                    It.IsAny<ScheduleModel>(),
                    It.IsAny<GardenTaskModel>(),
                    It.IsAny<bool>()))
                .Returns(() => true);

            var sut = CreateServiceUnderTest(mockRepo.Object,
                mockStartService.Object,
                mockStopService.Object,
                mockArduinoService.Object);

            // Act 
            var actual = await sut.CheckSchedules();
            // Assert 
            actual.Should().BeTrue();
            mockRepo.Verify();
            mockArduinoService.Verify();
            mockStartService.Verify();
            mockStopService.Verify();
        }


        [Fact]
        public async Task CheckSchedules_NotingToDo_ReturnsTrue() {
            // Arrange 
            var mockRepo = new Mock<IScheduleRepository>();
            mockRepo.Setup(m =>
                    m.GetSchedule(It.IsAny<ScheduleTypeEnum>()))
                .ReturnsAsync(() => new ScheduleModel {
                    Id = 1
                });

            var mockArduinoService = new Mock<IScheduleArduinoService>();
            mockArduinoService.Setup(a => a.GetLatestGardenTask(
                    It.IsAny<ScheduleModel>()))
                .ReturnsAsync(() => new GardenTaskModel());

            mockArduinoService.Setup(a =>
                    a.CheckIfScheduleIsRunning(It.IsAny<ScheduleModel>()))
                .ReturnsAsync(() => new RunningModel());

            mockArduinoService
                .Setup(a => a.ExecuteScheduleStopAction(It.IsAny<ScheduleModel>()))
                .ReturnsAsync(() => new TaskResult {
                    Success = true
                });

            var mockStartService = new Mock<IScheduleStartService>();
            mockStartService.Setup(s =>
                    s.CheckIfShouldBeStarted(It.IsAny<ScheduleModel>(),
                        It.IsAny<GardenTaskModel>(),
                        It.IsAny<bool>()))
                .Returns(() => false);


            var mockStopService = new Mock<IScheduleStopService>();
            mockStopService.Setup(m => m.CheckIfShouldBeStopped(
                    It.IsAny<ScheduleModel>(),
                    It.IsAny<GardenTaskModel>(),
                    It.IsAny<bool>()))
                .Returns(() => false);

            var sut = CreateServiceUnderTest(mockRepo.Object,
                mockStartService.Object,
                new Mock<IScheduleStopService>().Object,
                mockArduinoService.Object);

            // Act 
            var actual = await sut.CheckSchedules();
            // Assert 
            actual.Should().BeTrue();
            mockRepo.Verify();
            mockArduinoService.Verify();
            mockStartService.Verify();
            mockStopService.Verify();
        }


        private static ScheduleService CreateServiceUnderTest(IScheduleRepository scheduleRepository,
            IScheduleStartService scheduleStartService,
            IScheduleStopService scheduleStopService,
            IScheduleArduinoService scheduleArduinoService) {
            return new(scheduleRepository,
                scheduleArduinoService,
                scheduleStartService,
                scheduleStopService,
                NullLogger<ScheduleService>.Instance);
        }
    }
}