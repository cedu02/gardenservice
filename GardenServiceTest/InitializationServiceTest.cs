using System.Threading.Tasks;
using FluentAssertions;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using GardenService.Services;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Xunit;

namespace GardenServiceTest {
    public class InitializationServiceTest {
        [Fact]
        public async Task InitializeFixedDatabaseValues_Successful_ReturnsTrue() {
            // Arrange 
            var mockScheduleTypeRepo = new Mock<IScheduleTypeRepository>();
            mockScheduleTypeRepo.Setup(s =>
                    s.Upsert(It.IsAny<ScheduleTypeEnum>()))
                .ReturnsAsync(() => 1);

            var mockSensorTypeRepo = new Mock<ISensorTypeRepository>();
            mockSensorTypeRepo.Setup(s =>
                    s.Upsert(It.IsAny<SensorTypeEnum>()))
                .ReturnsAsync(() => 1);

            var mockSensorRepo = new Mock<ISensorRepository>();
            mockSensorRepo.Setup(s =>
                    s.Upsert(It.IsAny<SensorEnum>()))
                .ReturnsAsync(() => 1);

            var mockGardenTaskTypeRepo = new Mock<IGardenTaskTypeRepository>();
            mockGardenTaskTypeRepo.Setup(s =>
                    s.Upsert(It.IsAny<ArduinoTaskEnum>()))
                .ReturnsAsync(() => 1);


            var serviceUnderTest = CreateServiceUnderTest(mockScheduleTypeRepo.Object,
                mockSensorTypeRepo.Object, mockSensorRepo.Object, mockGardenTaskTypeRepo.Object);

            // Act
            var actual = await serviceUnderTest.InitializeFixedDatabaseValues();

            // Assert 
            actual.Should().BeTrue();
            mockSensorRepo.Verify();
            mockScheduleTypeRepo.Verify();
            mockSensorTypeRepo.Verify();
            mockGardenTaskTypeRepo.Verify();
        }


        [Fact]
        public async Task InitializeFixedDatabaseValues_OneFailed_ReturnsFalse() {
            // Arrange 
            var mockScheduleTypeRepo = new Mock<IScheduleTypeRepository>();
            mockScheduleTypeRepo.Setup(s =>
                    s.Upsert(It.IsAny<ScheduleTypeEnum>()))
                .ReturnsAsync(() => 1);

            var mockSensorTypeRepo = new Mock<ISensorTypeRepository>();
            mockSensorTypeRepo.Setup(s =>
                    s.Upsert(It.IsAny<SensorTypeEnum>()))
                .ReturnsAsync(() => 0);

            var mockSensorRepo = new Mock<ISensorRepository>();
            mockSensorRepo.Setup(s =>
                    s.Upsert(It.IsAny<SensorEnum>()))
                .ReturnsAsync(() => 1);

            var mockGardenTaskTypeRepo = new Mock<IGardenTaskTypeRepository>();
            mockGardenTaskTypeRepo.Setup(s =>
                    s.Upsert(It.IsAny<ArduinoTaskEnum>()))
                .ReturnsAsync(() => 1);


            var serviceUnderTest = CreateServiceUnderTest(mockScheduleTypeRepo.Object,
                mockSensorTypeRepo.Object, mockSensorRepo.Object, mockGardenTaskTypeRepo.Object);

            // Act
            var actual = await serviceUnderTest.InitializeFixedDatabaseValues();

            // Assert 
            actual.Should().BeFalse();
            mockSensorRepo.Verify();
            mockScheduleTypeRepo.Verify();
            mockSensorTypeRepo.Verify();
            mockGardenTaskTypeRepo.Verify();
        }

        private static InitializationService CreateServiceUnderTest(
            IScheduleTypeRepository scheduleTypeRepository,
            ISensorTypeRepository sensorTypeRepository,
            ISensorRepository sensorRepository,
            IGardenTaskTypeRepository gardenTaskTypeRepository) {
            
            return new(NullLogger<InitializationService>.Instance,
                scheduleTypeRepository,
                sensorTypeRepository,
                sensorRepository,
                Mock.Of<IWaterLevelTypeRepository>(),
                gardenTaskTypeRepository);
        }
    }
}