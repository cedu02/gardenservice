using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Models;
using GardenService.Interfaces;
using GardenService.Models;
using GardenService.Services;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;
using Moq;
using Xunit;

namespace GardenServiceTest {
    public class ArduinoCommunicationServiceTest {
        /// <summary>
        ///     Test ExecuteTaskOnArduinoResult string serialization to objects.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task ExecuteTaskOnArduinoResult_RandomT_ReturnsT() {
            // Arrange 
            var mockSerialPortService = new Mock<ISerialPortService>();
            mockSerialPortService
                .Setup(s =>
                    s.SendAndReceive(It.IsAny<string>(), It.IsAny<int>(),
                        It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(() =>
                    "<{\"Success\": false, \"Message\": \"hola\"}>");

            var serviceUnderTest = CreateServiceUnderTest(mockSerialPortService.Object);
            // Act 
            var actual = await serviceUnderTest.ExecuteTaskOnArduinoResult<TaskResult>(
                ArduinoTaskEnum.InvalidAction, ArduinoEnum.ReservoirArduino);

            // Assert 
            Assert.False(actual.Success);
            Assert.Equal("hola", actual.Message);
            mockSerialPortService.Verify();
        }

        /// <summary>
        ///     Test ExecuteTaskOnArduinoResult when result of serial port is corrupted.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task ExecuteTaskOnArduinoResult_InvalidReturnOfSerialPort_ReturnsEmptyT() {
            // Arrange
            var mockSerialPortService = new Mock<ISerialPortService>();
            mockSerialPortService
                .Setup(s =>
                    s.SendAndReceive(It.IsAny<string>(), It.IsAny<int>(),
                        It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(() =>
                    "<{\"Sucladsfs, \"M>");
            // Act 
            var serviceUnderTest = CreateServiceUnderTest(mockSerialPortService.Object);
            // Act 
            var actual = await serviceUnderTest.ExecuteTaskOnArduinoResult<TaskResult>(
                ArduinoTaskEnum.InvalidAction, ArduinoEnum.ReservoirArduino);

            // Assert 
            Assert.False(actual.Success);
            Assert.Equal(string.Empty, actual.Message);
            mockSerialPortService.Verify();
        }


        /// <summary>
        ///     Test ExecuteTaskOnArduinoResult when result of serial port is corrupted.
        ///     The response doesnt contain the markers.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task ExecuteTaskOnArduinoResult_NoMarkerInReturn_ReturnsEmptyT() {
            // Arrange
            var mockSerialPortService = new Mock<ISerialPortService>();
            mockSerialPortService
                .Setup(s =>
                    s.SendAndReceive(It.IsAny<string>(), It.IsAny<int>(),
                        It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(() =>
                    "{\"fsaldkf, \"M");
            // Act 
            var serviceUnderTest = CreateServiceUnderTest(mockSerialPortService.Object);
            // Act 
            var actual = await serviceUnderTest.ExecuteTaskOnArduinoResult<TaskResult>(
                ArduinoTaskEnum.InvalidAction, ArduinoEnum.ReservoirArduino);

            // Assert 
            Assert.False(actual.Success);
            Assert.Equal(string.Empty, actual.Message);
            mockSerialPortService.Verify();
        }

        private static ArduinoCommunicationService CreateServiceUnderTest(
            ISerialPortService serialPortService) {
            var fakeOptionGrow = Options.Create(
                new GrowArduino());

            var fakeOptionReservoir = Options.Create(
                new ReservoirArduino());

            return new ArduinoCommunicationService(
                fakeOptionReservoir,
                fakeOptionGrow,
                serialPortService,
                new NullLogger<ArduinoCommunicationService>());
        }
    }
}