using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Models;
using GardenService.Interfaces;
using GardenService.Services;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Xunit;

namespace GardenServiceTest {
    public class TestArudinoService {
        [Fact]
        public async Task ExecuteTask_RandomObjectT_ReturnsT() {
            // Arrange 
            var expected = new TaskResult();

            var communicationServiceMock =
                new Mock<IArduinoCommunicationService>();

            communicationServiceMock.Setup(s =>
                    s.ExecuteTaskOnArduinoResult<TaskResult>(
                        It.IsAny<ArduinoTaskEnum>(), It.IsAny<ArduinoEnum>()))
                .ReturnsAsync(() => expected);

            var serviceUnderTest = CreateArduinoService(communicationServiceMock.Object);

            // Act 
            var actual =
                await serviceUnderTest.ExecuteTask<TaskResult>(ArduinoTaskEnum.InvalidAction, ArduinoEnum.GrowArduino);

            //Assert 
            Assert.Equal(expected, actual);
            communicationServiceMock.Verify();
        }


        /// <summary>
        ///     Create Service to test.
        /// </summary>
        /// <param name="communicationService">Communication Service Mock</param>
        /// <returns></returns>
        private ArduinoService CreateArduinoService(IArduinoCommunicationService communicationService) {
            return new(communicationService, new NullLogger<ArduinoService>());
        }
    }
}