using System.Threading.Tasks;
using FluentAssertions;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using GardenCore.Models;
using GardenService.Interfaces;
using GardenService.Services.Schedule;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Xunit;

namespace GardenServiceTest {
    public class ScheduleArduinoServiceTest {
        [Fact]
        public async Task ExecuteScheduleStartAction_Successful_ReturnsSuccessTaskResult() {
            // Arrange 
            var arduinoServiceMock = new Mock<IArduinoService>();
            arduinoServiceMock.Setup(a =>
                    a.ExecuteTask<TaskResult>(It.IsAny<ArduinoTaskEnum>(),
                        It.IsAny<ArduinoEnum>()))
                .ReturnsAsync(() => new TaskResult {
                    Success = true
                });
            var schedule = new ScheduleModel {
                ScheduleType = ScheduleTypeEnum.SwitchWaterPump
            };
            var serviceUnderTest = CreateServiceUnderTest(arduinoServiceMock.Object);
            // Act
            var actual = await serviceUnderTest.ExecuteScheduleStartAction(schedule);
            // Assert 
            actual.Success.Should().BeTrue();
            arduinoServiceMock.Verify();
        }

        [Fact]
        public async Task ExecuteScheduleStartAction_ArduinoFails_ReturnsFailedTaskResult() {
            // Arrange 
            const string failed = "Failed";
            var arduinoServiceMock = new Mock<IArduinoService>();
            arduinoServiceMock.Setup(a =>
                    a.ExecuteTask<TaskResult>(It.IsAny<ArduinoTaskEnum>(),
                        It.IsAny<ArduinoEnum>()))
                .ReturnsAsync(() => new TaskResult {
                    Success = false,
                    Message = failed
                });
            var schedule = new ScheduleModel {
                ScheduleType = ScheduleTypeEnum.SwitchWaterPump
            };
            var serviceUnderTest = CreateServiceUnderTest(arduinoServiceMock.Object);
            // Act
            var actual = await serviceUnderTest.ExecuteScheduleStartAction(schedule);
            // Assert 
            actual.Success.Should().BeFalse();
            actual.Message.Should().Be(failed);
            arduinoServiceMock.Verify();
        }


        [Fact]
        public async Task ExecuteScheduleStartAction_InvalidScheduleType_ReturnsFailedTaskResult() {
            // Arrange 
            var arduinoServiceMock = new Mock<IArduinoService>();
            var schedule = new ScheduleModel();
            var serviceUnderTest = CreateServiceUnderTest(arduinoServiceMock.Object);
            // Act
            var actual = await serviceUnderTest.ExecuteScheduleStartAction(schedule);
            // Assert 
            actual.Success.Should().BeFalse();
            actual.Message.Should().Be(ScheduleArduinoService.InvalidSchedule);
        }

        [Fact]
        public async Task ExecuteScheduleStopAction_Successful_ReturnsSuccessTaskResult() {
            // Arrange 
            var arduinoServiceMock = new Mock<IArduinoService>();
            arduinoServiceMock.Setup(a =>
                    a.ExecuteTask<TaskResult>(It.IsAny<ArduinoTaskEnum>(),
                        It.IsAny<ArduinoEnum>()))
                .ReturnsAsync(() => new TaskResult {
                    Success = true
                });
            var schedule = new ScheduleModel {
                ScheduleType = ScheduleTypeEnum.SwitchWaterPump
            };
            var serviceUnderTest = CreateServiceUnderTest(arduinoServiceMock.Object);
            // Act
            var actual = await serviceUnderTest.ExecuteScheduleStopAction(schedule);
            // Assert 
            actual.Success.Should().BeTrue();
            arduinoServiceMock.Verify();
        }

        [Fact]
        public async Task ExecuteScheduleStopAction_ArduinoFails_ReturnsFailedTaskResult() {
            // Arrange 
            const string failed = "Failed";
            var arduinoServiceMock = new Mock<IArduinoService>();
            arduinoServiceMock.Setup(a =>
                    a.ExecuteTask<TaskResult>(It.IsAny<ArduinoTaskEnum>(),
                        It.IsAny<ArduinoEnum>()))
                .ReturnsAsync(() => new TaskResult {
                    Success = false,
                    Message = failed
                });
            var schedule = new ScheduleModel {
                ScheduleType = ScheduleTypeEnum.SwitchWaterPump
            };
            var serviceUnderTest = CreateServiceUnderTest(arduinoServiceMock.Object);
            // Act
            var actual = await serviceUnderTest.ExecuteScheduleStopAction(schedule);
            // Assert 
            actual.Success.Should().BeFalse();
            actual.Message.Should().Be(failed);
            arduinoServiceMock.Verify();
        }


        [Fact]
        public async Task ExecuteScheduleStopAction_InvalidScheduleType_ReturnsFailedTaskResult() {
            // Arrange 
            var arduinoServiceMock = new Mock<IArduinoService>();
            var schedule = new ScheduleModel();
            var serviceUnderTest = CreateServiceUnderTest(arduinoServiceMock.Object);
            // Act
            var actual = await serviceUnderTest.ExecuteScheduleStopAction(schedule);
            // Assert 
            actual.Success.Should().BeFalse();
            actual.Message.Should().Be(ScheduleArduinoService.InvalidSchedule);
        }


        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public async Task CheckIfScheduleIsRunning_Successful_ReturnsSuccessRunningModel(bool value) {
            // Arrange 
            var arduinoServiceMock = new Mock<IArduinoService>();
            arduinoServiceMock.Setup(a =>
                    a.ExecuteTask<RunningModel>(It.IsAny<ArduinoTaskEnum>(),
                        It.IsAny<ArduinoEnum>()))
                .ReturnsAsync(() => new RunningModel {
                    Running = value
                });
            var schedule = new ScheduleModel {
                ScheduleType = ScheduleTypeEnum.SwitchWaterPump
            };
            var serviceUnderTest = CreateServiceUnderTest(arduinoServiceMock.Object);
            // Act
            var actual = await serviceUnderTest.CheckIfScheduleIsRunning(schedule);
            // Assert 
            actual.Running.Should().Be(value);
            arduinoServiceMock.Verify();
        }

        [Fact]
        public async Task CheckIfScheduleIsRunning_InvalidScheduleType_ReturnsFailedRunningModel() {
            // Arrange 
            var arduinoServiceMock = new Mock<IArduinoService>();
            var schedule = new ScheduleModel();
            var serviceUnderTest = CreateServiceUnderTest(arduinoServiceMock.Object);
            // Act
            var actual = await serviceUnderTest.CheckIfScheduleIsRunning(schedule);
            // Assert 
            actual.Running.Should().BeFalse();
        }


        [Fact]
        public async Task GetLatestGardenTask_ValidSchedule_ReturnsLatestExecutedTask() {
            // Arrange 
            var repoMock = new Mock<IGardenTaskRepository>();
            repoMock.Setup(r => r.GetLatestActionForSchedule(
                    It.IsAny<ScheduleModel>()))
                .ReturnsAsync(() => new GardenTaskModel {
                    ArduinoTask = ArduinoTaskEnum.StartLight
                });

            var schedule = new ScheduleModel();
            var serviceUnderTest = CreateServiceUnderTest(repoMock.Object);
            // Act
            var actual = await serviceUnderTest.GetLatestGardenTask(schedule);
            // Assert 
            actual.ArduinoTask.Should().Be(ArduinoTaskEnum.StartLight);
            repoMock.Verify();
        }


        /// <summary>
        ///     Create ScheduleArduinoService with fake logger and
        ///     provided IArduinoService Mock or Stub.
        /// </summary>
        /// <param name="arduinoService">Mock or Stub of ArduinoService</param>
        /// <returns></returns>
        private static ScheduleArduinoService CreateServiceUnderTest(
            IArduinoService arduinoService) {
            return new(NullLogger<ScheduleArduinoService>.Instance,
                arduinoService,
                new Mock<IGardenTaskRepository>().Object);
        }


        /// <summary>
        ///     Create ScheduleArduinoService with fake logger and
        ///     provided IGardenTaskRepository Mock or Stub.
        /// </summary>
        /// <param name="gardenTaskRepository">Mock or Stub of gardenTaskRepository</param>
        /// <returns></returns>
        private static ScheduleArduinoService CreateServiceUnderTest(IGardenTaskRepository gardenTaskRepository) {
            return new(NullLogger<ScheduleArduinoService>.Instance,
                new Mock<IArduinoService>().Object,
                gardenTaskRepository);
        }
    }
}