using System;
using System.Threading.Tasks;
using FluentAssertions;
using GardenCore.Enums;
using GardenCore.Interfaces.Services.Cache;
using GardenService.Interfaces;
using GardenService.Services;
using GardenServiceTest.Stub;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Xunit;

namespace GardenServiceTest {
    public class SensorDataCollectorTest {
        [Fact]
        public async Task CollectSensorData_FailsToReadService_ReturnsFalse() {
            // Arrange 
            var arduinoServiceMock = new Mock<IArduinoService>();
            arduinoServiceMock.Setup(a =>
                    a.ExecuteTask<It.IsAnyType>(It.IsAny<ArduinoTaskEnum>(),
                        It.IsAny<ArduinoEnum>()))
                .ThrowsAsync(new Exception());

            var sut = CreateServiceUnderTest(new Mock<ISensorCacheService>().Object,
                arduinoServiceMock.Object);

            // Act
            var actual = await sut.CollectSensorData();

            // Assert 
            actual.Success.Should().BeFalse();
        }


        [Fact]
        public async Task CollectSensorData_FailsToSaveToCacheWithException_ReturnsFalse() {
            // Arrange 
            var arduinoServiceStub = new ArduinoServiceStub();
            var sensorCacheServiceMock = new Mock<ISensorCacheService>();
            sensorCacheServiceMock.Setup(s =>
                    s.SetValue(It.IsAny<SensorEnum>(), It.IsAny<It.IsAnyType>()))
                .ThrowsAsync(new Exception());

            var sut = CreateServiceUnderTest(sensorCacheServiceMock.Object,
                arduinoServiceStub);

            // Act
            var actual = await sut.CollectSensorData();

            // Assert 
            actual.Success.Should().BeFalse();
            sensorCacheServiceMock.Verify();
        }


        [Fact]
        public async Task CollectSensorData_FailsToSaveToCache_ReturnsFalse() {
            // Arrange 
            var arduinoServiceStub = new ArduinoServiceStub();
            var sensorCacheServiceMock = new Mock<ISensorCacheService>();
            sensorCacheServiceMock.Setup(s =>
                    s.SetValue(It.IsAny<SensorEnum>(), It.IsAny<It.IsAnyType>()))
                .ReturnsAsync(() => false);

            var sut = CreateServiceUnderTest(sensorCacheServiceMock.Object,
                arduinoServiceStub);

            // Act
            var actual = await sut.CollectSensorData();

            // Assert 
            actual.Success.Should().BeFalse();
            sensorCacheServiceMock.Verify();
        }


        [Fact]
        public async Task CollectSensorData_Successful_ReturnsTrue() {
            // Arrange 
            var arduinoServiceStub = new ArduinoServiceStub();
            var sensorCacheServiceMock = new Mock<ISensorCacheService>();
            sensorCacheServiceMock.Setup(s =>
                    s.SetValue(It.IsAny<SensorEnum>(), It.IsAny<It.IsAnyType>()))
                .ReturnsAsync(() => true);

            var sut = CreateServiceUnderTest(sensorCacheServiceMock.Object,
                arduinoServiceStub);

            // Act
            var actual = await sut.CollectSensorData();

            // Assert 
            actual.Success.Should().BeTrue();
            sensorCacheServiceMock.Verify();
        }


        private static SensorDataCollector CreateServiceUnderTest(ISensorCacheService sensorCacheService,
            IArduinoService arduinoService) {
            return new(sensorCacheService,
                arduinoService,
                NullLogger<SensorDataCollector>.Instance);
        }
    }
}