using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;

namespace GardenServiceTest.Stub {
    public class SensorDataRepositoryStub : ISensorDataRepository {
        public async Task<long> SaveNewData(SensorEnum sensor, object data) {
            return 1;
        }
    }
}