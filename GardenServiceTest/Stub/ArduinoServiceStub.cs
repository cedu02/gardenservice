using System.Threading.Tasks;
using GardenCore.Enums;
using GardenService.Interfaces;

namespace GardenServiceTest.Stub {
    public class ArduinoServiceStub : IArduinoService {
        public async Task<T> ExecuteTask<T>(ArduinoTaskEnum action, ArduinoEnum arduino) where T : new() {
            return new();
        }
    }
}