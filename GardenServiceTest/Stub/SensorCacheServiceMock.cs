using System;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Interfaces.Services.Cache;

namespace GardenServiceTest.Stub {
    public class SensorCacheServiceMock : ISensorCacheService {
        public async Task<T> GetValue<T>(SensorEnum sensorEnum) where T : new() {
            return new();
        }

        public Task<bool> SetValue<T>(SensorEnum sensorEnum, T value) {
            throw new NotImplementedException();
        }
    }
}