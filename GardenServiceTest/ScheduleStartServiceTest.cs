using System;
using FluentAssertions;
using GardenCore.Enums;
using GardenCore.Models;
using GardenService.Services.Schedule;
using Microsoft.Extensions.Logging.Abstractions;
using Xunit;

namespace GardenServiceTest {
    public class ScheduleStartServiceTest {
        [Fact]
        public void CheckIfShouldBeStarted_AlreadyRunning_ReturnsFalse() {
            // Arrange 
            var sut = CreateServiceUnderTest();
            var lastTask = new GardenTaskModel {
                ArduinoTask = ArduinoTaskEnum.StartFan
            };
            var schedule = new ScheduleModel {
                ScheduleType = ScheduleTypeEnum.SwitchFan
            };
            const bool isRunning = true;
            // Act
            var actual = sut.CheckIfShouldBeStarted(schedule, lastTask, isRunning);

            // Assert
            actual.Should().BeFalse();
        }


        [Fact]
        public void CheckIfShouldBeStarted_TimeToStartSchedule_ReturnsTrue() {
            // Arrange 
            var sut = CreateServiceUnderTest();
            const float interval = (float) 1.2;
            var lastTask = new GardenTaskModel {
                ArduinoTask = ArduinoTaskEnum.StopFan,
                Timestamp = DateTime.Now.AddHours(-(interval - 1))
            };
            var schedule = new ScheduleModel {
                ScheduleType = ScheduleTypeEnum.SwitchFan,
                Interval = interval
            };
            const bool isRunning = false;
            // Act
            var actual = sut.CheckIfShouldBeStarted(schedule, lastTask, isRunning);
            // Assert
            actual.Should().BeTrue();
        }


        [Fact]
        public void CheckIfShouldBeStarted_NotTimeToStartSchedule_ReturnsFalse() {
            // Arrange 
            var sut = CreateServiceUnderTest();
            const float interval = (float) 1.2;
            var lastTask = new GardenTaskModel {
                ArduinoTask = ArduinoTaskEnum.StopFan,
                Timestamp = DateTime.Now.AddHours(-(2 * interval))
            };
            var schedule = new ScheduleModel {
                ScheduleType = ScheduleTypeEnum.SwitchFan,
                Interval = interval
            };
            const bool isRunning = false;
            // Act
            var actual = sut.CheckIfShouldBeStarted(schedule, lastTask, isRunning);
            // Assert
            actual.Should().BeFalse();
        }

        [Fact]
        public void CheckIfShouldBeStarted_NotRunningButShould_ReturnsFalse() {
            // Arrange 
            var sut = CreateServiceUnderTest();
            var lastTask = new GardenTaskModel {
                ArduinoTask = ArduinoTaskEnum.StartFan
            };
            var schedule = new ScheduleModel {
                ScheduleType = ScheduleTypeEnum.SwitchFan
            };
            const bool isRunning = false;
            // Act
            var actual = sut.CheckIfShouldBeStarted(schedule, lastTask, isRunning);
            // Assert
            actual.Should().BeFalse();
        }

        private ScheduleStartService CreateServiceUnderTest() {
            return new(NullLogger<ScheduleStartService>.Instance);
        }
    }
}