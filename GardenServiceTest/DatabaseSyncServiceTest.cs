using System.Threading.Tasks;
using FluentAssertions;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using GardenCore.Interfaces.Services.Cache;
using GardenCore.Models.SensorData;
using GardenService.Services;
using GardenServiceTest.Stub;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Xunit;

namespace GardenServiceTest {
    public class DatabaseSyncServiceTest {
        [Fact]
        public async Task SaveSensorDataToDB_ValidData_ReturnTrue() {
            // Arrange 
            var factoryMock = new Mock<ISensorDataRepositoryFactory>();
            factoryMock
                .Setup(f => f.CreateRepository(It.IsAny<SensorEnum>()))
                .Returns(() => new SensorDataRepositoryStub());

            var cacheServiceMock = new SensorCacheServiceMock();

            var sut = CreateSUT(factoryMock.Object, cacheServiceMock);

            // Act
            var actual = await sut.SaveSensorDataToDb();
            actual.Should().BeTrue();
            factoryMock.Verify();
        }


        [Fact]
        public async Task SaveSensorDataToDB_InvalidData_ReturnFalse() {
            // Arrange 
            var factoryMock = new Mock<ISensorDataRepositoryFactory>();
            factoryMock
                .Setup(f => f.CreateRepository(It.IsAny<SensorEnum>()))
                .Returns(() => new SensorDataRepositoryStub());

            var cacheServiceMock = new Mock<ISensorCacheService>();
            cacheServiceMock.Setup(s => s.GetValue<PHModel>(It.IsAny<SensorEnum>()))
                .ReturnsAsync(() => null);

            var sut = CreateSUT(factoryMock.Object, cacheServiceMock.Object);

            // Act
            var actual = await sut.SaveSensorDataToDb();
            actual.Should().BeFalse();
            factoryMock.Verify();
            cacheServiceMock.Verify();
        }


        [Fact]
        public async Task SaveSensorDataToDB_InvalidRepository_ReturnFalse() {
            // Arrange 
            var factoryMock = new Mock<ISensorDataRepositoryFactory>();
            factoryMock
                .Setup(f => f.CreateRepository(It.IsAny<SensorEnum>()))
                .Returns(() => null);

            var cacheServiceMock = new SensorCacheServiceMock();

            var sut = CreateSUT(factoryMock.Object, cacheServiceMock);

            // Act
            var actual = await sut.SaveSensorDataToDb();
            actual.Should().BeFalse();
            factoryMock.Verify();
        }

        private static DatabaseSyncService CreateSUT(ISensorDataRepositoryFactory repositoryFactory,
            ISensorCacheService sensor) {
            return new(NullLogger<DatabaseSyncService>.Instance,
                repositoryFactory,
                sensor);
        }
    }
}