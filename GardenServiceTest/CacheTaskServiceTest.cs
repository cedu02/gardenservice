using System.Threading.Tasks;
using FluentAssertions;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using GardenCore.Models;
using GardenDB.Cache.Interfaces;
using GardenDB.Cache.Services;
using GardenService.Interfaces;
using GardenService.Services;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Xunit;

namespace GardenServiceTest {
    public class CacheTaskServiceTest {
        [Fact]
        public async Task CheckForOpenTasksInCache_NoOpenTasks_ReturnsTrue() {
            // Arrange 
            var mockTaskCollector = new Mock<ITaskCollector>();
            mockTaskCollector.Setup(t => t.GetSwitchTask(It.IsAny<GardenTaskEnum>()))
                .ReturnsAsync(() => new TaskModel {
                    TaskFound = false
                });
            var stubArduinoService = new Mock<IArduinoService>();
            var stubGardenTaskRep = new Mock<IGardenTaskRepository>();
            var serviceUnderTest = CreateServiceUnderTest(stubArduinoService.Object,
                mockTaskCollector.Object,
                stubGardenTaskRep.Object);

            // Act 
            var actual = await serviceUnderTest.CheckForOpenTasksInCache();
            // Assert 
            actual.Should().BeTrue();
            mockTaskCollector.Verify();
        }


        [Fact]
        public async Task CheckForOpenTasksInCache_OpenTaskFailedToExecute_ReturnsFalse() {
            // Arrange 
            var mockTaskCollector = new Mock<ITaskCollector>();
            mockTaskCollector.Setup(t => t.GetSwitchTask(It.IsAny<GardenTaskEnum>()))
                .ReturnsAsync(() => new TaskModel {
                    TaskFound = true,
                    StatusToChangeTo = false
                });
            var mockArduinoService = new Mock<IArduinoService>();
            mockArduinoService.Setup(a =>
                    a.ExecuteTask<TaskResult>(It.IsAny<ArduinoTaskEnum>(), It.IsAny<ArduinoEnum>()))
                .ReturnsAsync(() => new TaskResult {
                    Success = false
                });
            var mockGardenTaskRep = new Mock<IGardenTaskRepository>();
            var serviceUnderTest = CreateServiceUnderTest(mockArduinoService.Object,
                mockTaskCollector.Object,
                mockGardenTaskRep.Object);

            // Act 
            var actual = await serviceUnderTest.CheckForOpenTasksInCache();
            // Assert 
            actual.Should().BeFalse();
        }


        [Fact]
        public async Task CheckForOpenTasksInCache_OpenTaskSuccessfulExecution_ReturnsTrue() {
            // Arrange 
            var mockTaskCollector = new Mock<ITaskCollector>();
            mockTaskCollector.Setup(t => t.GetSwitchTask(It.IsAny<GardenTaskEnum>()))
                .ReturnsAsync(() => new TaskModel {
                    TaskFound = true,
                    StatusToChangeTo = false
                });
            var mockArduinoService = new Mock<IArduinoService>();
            mockArduinoService.Setup(a =>
                    a.ExecuteTask<TaskResult>(It.IsAny<ArduinoTaskEnum>(), It.IsAny<ArduinoEnum>()))
                .ReturnsAsync(() => new TaskResult {
                    Success = true
                });
            var mockGardenTaskRep = new Mock<IGardenTaskRepository>();
            mockGardenTaskRep.Setup(g => g.SaveAction(It.IsAny<GardenTaskModel>()))
                .ReturnsAsync(() => new TaskResult());
            var serviceUnderTest = CreateServiceUnderTest(mockArduinoService.Object,
                mockTaskCollector.Object,
                mockGardenTaskRep.Object);

            // Act 
            var actual = await serviceUnderTest.CheckForOpenTasksInCache();
            // Assert 
            actual.Should().BeTrue();
            mockArduinoService.Verify();
            mockTaskCollector.Verify();
            mockGardenTaskRep.Verify();
        }

        [Fact]
        public async Task CheckForOpenTasksInCache_OpenTaskOneTaskFails_ReturnsTrue() {
            // Arrange 
            var mockTaskCollector = new Mock<ITaskCollector>();
            mockTaskCollector.Setup(t => t.GetSwitchTask(It.IsAny<GardenTaskEnum>()))
                .ReturnsAsync(() => new TaskModel {
                    TaskFound = true,
                    StatusToChangeTo = false
                });
            var mockArduinoService = new Mock<IArduinoService>();
            mockArduinoService.SetupSequence(a =>
                    a.ExecuteTask<TaskResult>(It.IsAny<ArduinoTaskEnum>(), It.IsAny<ArduinoEnum>()))
                .ReturnsAsync(() => new TaskResult {
                    Success = true
                })
                .ReturnsAsync(() => new TaskResult {
                    Success = true
                })
                .ReturnsAsync(() => new TaskResult {
                    Success = true
                })
                .ReturnsAsync(() => new TaskResult {
                    Success = false
                });
            var mockGardenTaskRep = new Mock<IGardenTaskRepository>();
            mockGardenTaskRep.Setup(g => g.SaveAction(It.IsAny<GardenTaskModel>()))
                .ReturnsAsync(() => new TaskResult());
            var serviceUnderTest = CreateServiceUnderTest(mockArduinoService.Object,
                mockTaskCollector.Object,
                mockGardenTaskRep.Object);

            // Act 
            var actual = await serviceUnderTest.CheckForOpenTasksInCache();
            // Assert 
            actual.Should().BeFalse();
            mockArduinoService.Verify();
            mockTaskCollector.Verify();
            mockGardenTaskRep.Verify();
        }


        /// <summary>
        ///     Create new instance of CacheTaskService.
        /// </summary>
        /// <param name="arduinoService"></param>
        /// <param name="taskCollector"></param>
        /// <param name="gardenTaskRepository"></param>
        /// <returns></returns>
        private static CacheTaskService CreateServiceUnderTest(IArduinoService arduinoService,
            ITaskCollector taskCollector,
            IGardenTaskRepository gardenTaskRepository) {
            return new(taskCollector,
                new NullLogger<CacheTaskService>(),
                gardenTaskRepository,
                arduinoService);
        }
    }
}