using System;
using System.Threading;
using System.Threading.Tasks;
using GardenService.Services;
using GardenService.Services.Schedule;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace GardenService {
    public class Worker : BackgroundService {
        private readonly CacheTaskService _cacheTaskService;
        private readonly DatabaseSyncService _databaseSyncService;
        private readonly InitializationService _initializationService;

        private readonly ILogger<Worker> _logger;
        private readonly ScheduleService _scheduleService;
        private readonly SensorDataCollector _sensorDataCollector;

        public Worker(ILogger<Worker> logger,
            CacheTaskService cacheTaskService,
            ScheduleService scheduleService,
            InitializationService initializationService,
            DatabaseSyncService databaseSyncService,
            SensorDataCollector sensorDataCollector) {
            _logger = logger;
            _initializationService = initializationService;
            _scheduleService = scheduleService;
            _cacheTaskService = cacheTaskService;
            _sensorDataCollector = sensorDataCollector;
            _databaseSyncService = databaseSyncService;
        }

        public override async Task StartAsync(CancellationToken stoppingToken) {
            await _initializationService.InitializeFixedDatabaseValues();
            await ExecuteAsync(stoppingToken);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken) {
            var lastDataSaveToDb = DateTime.MinValue.AddSeconds(10);
            var lastSensorDataCollection = DateTime.MinValue.AddSeconds(10);

            while (!stoppingToken.IsCancellationRequested) {
                _logger.LogDebug("Worker running at: {Time}", DateTimeOffset.Now);

                // _logger.LogDebug("Check schedules");
                // var result = await _scheduleService.CheckSchedules();
                // _logger.LogDebug("Schedules checked. {Result}", result);

                _logger.LogDebug("Check for External Task");
                var cacheCheckResult = await _cacheTaskService.CheckForOpenTasksInCache();
                _logger.LogDebug("Schedules checked. {CacheCheckResult}", cacheCheckResult);

                // if ((DateTime.Now - lastSensorDataCollection).Seconds >= 5) {
                //     _logger.LogDebug("Collect sensor data");
                //     var collectSensorData = await _sensorDataCollector.CollectSensorData();
                //     
                //     _logger.LogDebug("Collected sensor data. {Result}",
                //         collectSensorData.ToString());
                //     
                //     lastSensorDataCollection = DateTime.Now;
                // }

                // if ((DateTime.Now - lastDataSaveToDb).Minutes >= 1) {
                //     _logger.LogDebug("Save sensor data to db");
                //     var dbSyncResult = await _databaseSyncService.SaveSensorDataToDb();
                //     _logger.LogDebug("Saved sensor data to db. Result {Result}", dbSyncResult);
                //     lastDataSaveToDb = DateTime.Now;
                // }

                await Task.Delay(1000, stoppingToken);
            }
        }
    }
}