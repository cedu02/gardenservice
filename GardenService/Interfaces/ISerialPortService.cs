using System.Threading.Tasks;

namespace GardenService.Interfaces {
    public interface ISerialPortService {
        /// <summary>
        ///     Send task number to a serial port and wait for answer.
        /// </summary>
        /// <param name="port">Port of serial port.</param>
        /// <param name="taskNumber">Task identifier.</param>
        /// <param name="startMarker">Start marker in response of serial port.</param>
        /// <param name="stopMarker">Start marker in response of serial port.</param>
        /// <returns>Response of serial port as string.</returns>
        public Task<string> SendAndReceive(string port,
            int taskNumber, string startMarker, string stopMarker);
        
        /// <summary>
        /// Check if is available.
        /// </summary>
        /// <param name="port"></param>
        /// <returns></returns>
        public bool IsAvailable(string port);
    }
    
    
    
}