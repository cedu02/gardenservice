using System.Threading.Tasks;
using GardenCore.Enums;

namespace GardenService.Interfaces {
    public interface IArduinoCommunicationService {
        public Task<string> ExecuteTaskOnArduino(
            ArduinoTaskEnum action, ArduinoEnum arduino);

        public Task<T> ExecuteTaskOnArduinoResult<T>(
            ArduinoTaskEnum action, ArduinoEnum arduino) where T : new();
    }
}