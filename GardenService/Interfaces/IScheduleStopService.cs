using GardenCore.Models;

namespace GardenService.Interfaces {
    public interface IScheduleStopService {
        /// <summary>
        ///     Check if schedule should be stopped.
        /// </summary>
        /// <param name="schedule"></param>
        /// <param name="lastGardenTask"></param>
        /// <param name="isRunning"></param>
        /// <returns></returns>
        public bool CheckIfShouldBeStopped(ScheduleModel schedule,
            GardenTaskModel lastGardenTask,
            bool isRunning);
    }
}