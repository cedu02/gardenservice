using GardenCore.Models;

namespace GardenService.Interfaces {
    public interface IScheduleStartService {
        /// <summary>
        ///     Check if a schedule should be started.
        /// </summary>
        /// <param name="schedule"></param>
        /// <param name="lastGardenTask"></param>
        /// <param name="isRunning"></param>
        /// <returns></returns>
        public bool CheckIfShouldBeStarted(ScheduleModel schedule,
            GardenTaskModel lastGardenTask,
            bool isRunning);
    }
}