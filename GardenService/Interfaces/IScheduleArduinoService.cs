using System.Threading.Tasks;
using GardenCore.Models;

namespace GardenService.Interfaces {
    public interface IScheduleArduinoService {
        /// <summary>
        ///     Execute start action of schedule.
        /// </summary>
        /// <param name="schedule">Schedule which should be started.</param>
        /// <returns></returns>
        public Task<TaskResult> ExecuteScheduleStartAction(ScheduleModel schedule);


        /// <summary>
        ///     Execute stop action of schedule.
        /// </summary>
        /// <param name="schedule">Schedule which should be stopped. </param>
        /// <returns></returns>
        public Task<TaskResult> ExecuteScheduleStopAction(ScheduleModel schedule);

        /// <summary>
        ///     Check if schedule is running.
        /// </summary>
        /// <param name="schedule">Schedule to check</param>
        /// <returns></returns>
        public Task<RunningModel> CheckIfScheduleIsRunning(ScheduleModel schedule);


        /// <summary>
        ///     Get the last executed garden task for schedule.
        /// </summary>
        /// <param name="schedule">Schedule to check.</param>
        /// <returns></returns>
        public Task<GardenTaskModel> GetLatestGardenTask(ScheduleModel schedule);
    }
}