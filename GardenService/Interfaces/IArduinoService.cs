using System.Threading.Tasks;
using GardenCore.Enums;

namespace GardenService.Interfaces {
    public interface IArduinoService {
        /// <summary>
        ///     Sends Task to certain Arduino.
        /// </summary>
        /// <param name="action">Task to execute.</param>
        /// <param name="arduino">Arduino which should execute the task.</param>
        /// <typeparam name="T">Type of response expected from arduino.</typeparam>
        /// <returns></returns>
        public Task<T> ExecuteTask<T>(ArduinoTaskEnum action, ArduinoEnum arduino) where T : new();
    }
}