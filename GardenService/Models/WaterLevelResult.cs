using GardenCore.Enums;

namespace GardenService.Models {
    public class WaterLevelResult {
        public WaterLevels WaterLevel { get; set; } = WaterLevels.Empty;
    }
}