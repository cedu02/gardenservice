using GardenCore.Enums;

namespace GardenService.Models {
    public class SwitchResults {
        public GardenTaskEnum SwitchTask { get; set; } = GardenTaskEnum.None;
        public bool Success { get; set; }
    }
}