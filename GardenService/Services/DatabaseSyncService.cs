using System.Collections.Generic;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using GardenCore.Interfaces.Services.Cache;
using Microsoft.Extensions.Logging;

namespace GardenService.Services {
    public class DatabaseSyncService {
        private readonly ILogger<DatabaseSyncService> _logger;
        private readonly ISensorDataRepositoryFactory _repositoryFactory;
        private readonly ISensorCacheService _sensorCacheService;

        private readonly List<SensorEnum> _sensors = SensorEnum.GetAllSensors();

        public DatabaseSyncService(ILogger<DatabaseSyncService> logger,
            ISensorDataRepositoryFactory repositoryFactory,
            ISensorCacheService sensorCacheService) {
            _logger = logger;
            _repositoryFactory = repositoryFactory;
            _sensorCacheService = sensorCacheService;
        }

        public async Task<bool> SaveSensorDataToDb() {
            var saveResults = new List<bool>();

            foreach (var sensor in _sensors) {
                var method = _sensorCacheService.GetType().GetMethod(nameof(_sensorCacheService.GetValue));
                var generic = method?.MakeGenericMethod(sensor.DataTask.DataType);
                if (generic == null) {
                    _logger.LogError("Failed to construct cache service for datatype {type}",
                        sensor.DataTask.DataType);
                    saveResults.Add(false);
                    continue;
                }

                dynamic result = generic.Invoke(_sensorCacheService,
                    new object[] {sensor})!;
                var data = await result;

                if (data == null || data.GetType() != sensor.DataTask.DataType) {
                    var typeName = data == null ? "null" : (string) data.GetType().ToString();
                    _logger.LogError("Wrong data type of cache data. Expected: {type1} -> Actual: {type2}",
                        sensor.DataTask.DataType.ToString(), typeName);
                    saveResults.Add(false);
                    continue;
                }

                var repository = _repositoryFactory.CreateRepository(sensor);
                if (repository == null) {
                    _logger.LogError("Failed to create repository {repo}", sensor.Repository);
                    saveResults.Add(false);
                    continue;
                }

                var savedItem = await repository.SaveNewData(sensor, data);

                if (savedItem != 0 && savedItem != null) {
                    saveResults.Add(true);
                }
                else {
                    _logger.LogError("Result of Save New Data is invalid");
                    saveResults.Add(false);
                }
            }

            return saveResults.TrueForAll(x => x);
        }
    }
}