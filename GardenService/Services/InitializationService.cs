using System.Collections.Generic;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using Microsoft.Extensions.Logging;

namespace GardenService.Services {
    public class InitializationService {
        private readonly IGardenTaskTypeRepository _gardenTaskTypeRepository;

        private readonly ILogger<InitializationService> _logger;
        private readonly IScheduleTypeRepository _scheduleTypeRepository;
        private readonly ISensorRepository _sensorRepository;
        private readonly ISensorTypeRepository _sensorTypeRepository;
        private readonly IWaterLevelTypeRepository _waterLevelTypeRepository; 

        public InitializationService(ILogger<InitializationService> logger,
            IScheduleTypeRepository scheduleTypeRepository,
            ISensorTypeRepository sensorTypeRepository,
            ISensorRepository sensorRepository,
            IWaterLevelTypeRepository waterLevelTypeRepository,
            IGardenTaskTypeRepository gardenTaskTypeRepository) {
            
            _logger = logger;
            _scheduleTypeRepository = scheduleTypeRepository;
            _gardenTaskTypeRepository = gardenTaskTypeRepository;
            _sensorTypeRepository = sensorTypeRepository;
            _sensorRepository = sensorRepository;
            _waterLevelTypeRepository = waterLevelTypeRepository; 
        }

        public async Task<bool> InitializeFixedDatabaseValues() {
            var resultList = new List<long>();
            _logger.LogDebug("Create or update valid schedule types in db");
            foreach (var validTask in ScheduleTypeEnum.GetAllValidScheduleTypes())
                resultList.Add(await _scheduleTypeRepository.Upsert(validTask));

            _logger.LogDebug("Create or update all logged action types in db");
            foreach (var loggedActionType in ArduinoTaskEnum.GetLoggedActionTypes())
                resultList.Add(await _gardenTaskTypeRepository.Upsert(loggedActionType));

            _logger.LogDebug("Create or update all sensor types in db");
            foreach (var sensorType in SensorTypeEnum.GetSensorTypes())
                resultList.Add(await _sensorTypeRepository.Upsert(sensorType));


            _logger.LogDebug("Create or update all sensors in db");
            foreach (var sensor in SensorEnum.GetAllSensors()) resultList.Add(await _sensorRepository.Upsert(sensor));

            _logger.LogDebug("Create or update water levels");
            await  _waterLevelTypeRepository.Upsert(WaterLevels.Empty);
            await  _waterLevelTypeRepository.Upsert(WaterLevels.None);
            await  _waterLevelTypeRepository.Upsert(WaterLevels.HalveFull);
            await  _waterLevelTypeRepository.Upsert(WaterLevels.Full);
            await  _waterLevelTypeRepository.Upsert(WaterLevels.NearlyEmpty);
            
            return resultList.TrueForAll(i => i > 0);
        }
    }
}