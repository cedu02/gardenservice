using System;
using GardenCore.Models;
using GardenService.Interfaces;
using Microsoft.Extensions.Logging;

namespace GardenService.Services.Schedule {
    public class ScheduleStartService : IScheduleStartService {
        private readonly ILogger<ScheduleStartService> _logger;

        public ScheduleStartService(ILogger<ScheduleStartService> logger) {
            _logger = logger;
        }

        /// <summary>
        ///     Check if start task should be send to arduino based on schedule.
        /// </summary>
        /// <param name="schedule">Schedule information.</param>
        /// <param name="lastGardenTask">Last executed Action for schedule.</param>
        /// <param name="isRunning">Is the Part running or not at the moment.</param>
        /// <returns></returns>
        public bool CheckIfShouldBeStarted(ScheduleModel schedule,
            GardenTaskModel lastGardenTask,
            bool isRunning) {
            // When last action was a start and part is in running state on Arduino dont start again.
            if (lastGardenTask.ArduinoTask.ActionCode == schedule.ScheduleType.StartAction.ActionCode && isRunning) {
                _logger.LogDebug("Part is already running. Send start task: {result}",
                    false);
                return false;
            }

            // When last action was a stop action and the part is not running and 
            // the last action was executed interval times hours ago 
            // its time for the next start of the schedule.
            if (lastGardenTask.ArduinoTask.ActionCode == schedule.ScheduleType.StopAction.ActionCode &&
                !isRunning &&
                lastGardenTask.Timestamp.AddHours(schedule.Interval) >= DateTime.Now) {
                _logger.LogInformation("Next cycle in Schedule is reached. Send start task: {result}",
                    true);
                return true;
            }

            // When last action was a start and duration is not over yet but part is in 
            // not running state dont send start action but log a warning. 
            if (lastGardenTask.ArduinoTask.ActionCode == schedule.ScheduleType.StartAction.ActionCode &&
                !isRunning &&
                lastGardenTask.Timestamp.AddHours(schedule.Duration) < DateTime.Now) {
                _logger.LogWarning("Part should be running but is not. Send start task: {result}",
                    false);
                return false;
            }

            // for all other cases return false.
            _logger.LogDebug("Next cycle in Schedule not reached yet. Send start task: {result}",
                false);

            return false;
        }
    }
}