using System;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using GardenCore.Models;
using GardenService.Interfaces;
using Microsoft.Extensions.Logging;

namespace GardenService.Services.Schedule {
    public class ScheduleArduinoService : IScheduleArduinoService {
        public const string InvalidSchedule = "Invalid Schedule";
        private readonly IArduinoService _arduinoService;
        private readonly IGardenTaskRepository _gardenTaskRepository;
        private readonly ILogger<ScheduleArduinoService> _logger;

        public ScheduleArduinoService(ILogger<ScheduleArduinoService> logger,
            IArduinoService arduinoService,
            IGardenTaskRepository gardenTaskRepository) {
            _logger = logger;
            _arduinoService = arduinoService;
            _gardenTaskRepository = gardenTaskRepository;
        }

        public async Task<TaskResult> ExecuteScheduleStartAction(ScheduleModel schedule) {
            var scheduleCheck = CheckScheduleType(schedule);
            if (!scheduleCheck.Success) {
                _logger.LogWarning("Invalid schedule type provided {schedule}", schedule);
                return scheduleCheck;
            }

            _logger.LogDebug("Start Task has to be executed.");
            var result = await _arduinoService.ExecuteTask<TaskResult>(
                schedule.ScheduleType.StartAction,
                schedule.ScheduleType.StartAction.Arduino);

            if (result.Success)
                _logger.LogDebug("Start action executed. {action}",
                    schedule.ScheduleType.StartAction);
            else
                _logger.LogError("Start action failed: {action} - {result}",
                    schedule.ScheduleType.StartAction,
                    result);

            return result;
        }

        public async Task<TaskResult> ExecuteScheduleStopAction(ScheduleModel schedule) {
            var scheduleCheck = CheckScheduleType(schedule);
            if (!scheduleCheck.Success) {
                _logger.LogWarning("Invalid schedule type provided {schedule}", schedule);
                return scheduleCheck;
            }

            _logger.LogInformation("Stop action has to be executed.");
            var result = await _arduinoService.ExecuteTask<TaskResult>(
                schedule.ScheduleType.StopAction,
                schedule.ScheduleType.StopAction.Arduino);

            if (result.Success)
                _logger.LogDebug("Stop action executed. {action}",
                    schedule.ScheduleType.StopAction);
            else
                _logger.LogError("Stop action failed: {action} - {result}",
                    schedule.ScheduleType.StopAction,
                    result);
            return result;
        }

        public async Task<RunningModel> CheckIfScheduleIsRunning(ScheduleModel schedule) {
            var scheduleCheck = CheckScheduleType(schedule);
            if (!scheduleCheck.Success) {
                _logger.LogWarning("Invalid schedule type provided {schedule}", schedule);
                return new RunningModel();
            }

            _logger.LogDebug("Check state from Arduino {running}",
                schedule.ScheduleType.StateCheckAction);

            var isRunning = await _arduinoService.ExecuteTask<RunningModel>(
                schedule.ScheduleType.StateCheckAction,
                schedule.ScheduleType.StateCheckAction.Arduino);

            _logger.LogDebug("Is running? {isRunning}", isRunning);
            return isRunning;
        }

        public async Task<GardenTaskModel> GetLatestGardenTask(ScheduleModel schedule) {
            _logger.LogDebug("Get latest executed action of schedule {s}", schedule);
            var lastAction = await _gardenTaskRepository.GetLatestActionForSchedule(schedule);
            if (lastAction.Timestamp == DateTime.MinValue) _logger.LogDebug("No action for schedule found");
            return lastAction;
        }


        private static TaskResult CheckScheduleType(ScheduleModel schedule) {
            return new() {
                Success = schedule.ScheduleType != ScheduleTypeEnum.InvalidSchedule,
                Message = schedule.ScheduleType != ScheduleTypeEnum.InvalidSchedule ? "Ok" : InvalidSchedule
            };
        }
    }
}