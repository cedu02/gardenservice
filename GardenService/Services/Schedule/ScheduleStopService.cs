using System;
using GardenCore.Models;
using GardenService.Interfaces;
using Microsoft.Extensions.Logging;

namespace GardenService.Services.Schedule {
    public class ScheduleStopService : IScheduleStopService {
        private readonly ILogger<ScheduleStopService> _logger;

        public ScheduleStopService(ILogger<ScheduleStopService> logger) {
            _logger = logger;
        }

        public bool CheckIfShouldBeStopped(ScheduleModel schedule,
            GardenTaskModel lastGardenTask,
            bool isRunning) {
            // When the part was running for the time in the schedule.
            if (lastGardenTask.ArduinoTask.ActionCode == schedule.ScheduleType.StartAction.ActionCode &&
                isRunning &&
                lastGardenTask.Timestamp.AddHours(schedule.Duration) >= DateTime.Now)
                return true;

            if (lastGardenTask.ArduinoTask.ActionCode == schedule.ScheduleType.StopAction.ActionCode &&
                isRunning)
                _logger.LogWarning("Part shouldn't be running according to schedule {part}",
                    schedule.ScheduleType);

            _logger.LogDebug("Schedule is not started or end is not yet reached.");
            return false;
        }
    }
}