using System.Collections.Generic;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using GardenCore.Models;
using GardenService.Interfaces;
using Microsoft.Extensions.Logging;

namespace GardenService.Services.Schedule {
    public class ScheduleService {
        private readonly ILogger<ScheduleService> _logger;

        private readonly IScheduleArduinoService _scheduleArduinoService;
        private readonly IScheduleRepository _scheduleRepository;
        private readonly IScheduleStartService _scheduleStartService;
        private readonly IScheduleStopService _scheduleStopService;

        public ScheduleService(IScheduleRepository scheduleRepository,
            IScheduleArduinoService scheduleArduinoService,
            IScheduleStartService scheduleStartService,
            IScheduleStopService scheduleStopService,
            ILogger<ScheduleService> logger) {
            
            _scheduleRepository = scheduleRepository;
            _scheduleArduinoService = scheduleArduinoService;
            _logger = logger;
            _scheduleStartService = scheduleStartService;
            _scheduleStopService = scheduleStopService;
        }

        /// <summary>
        ///     Check Schedules for WaterPump, AirPump, Fan and Light.
        ///     If it needs to be started or stopped send Action to arduino.
        /// </summary>
        /// <returns></returns>
        public async Task<bool> CheckSchedules() {
            var schedules = ScheduleTypeEnum.GetAllValidScheduleTypes();
            var resultList = new List<TaskResult>();
            foreach (var schedule in schedules) {
                _logger.LogDebug("Check for {Name} task from schedule", schedule.Name);

                var checkResult = await CheckForActionOfScheduleType(schedule);
                resultList.Add(checkResult);

                _logger.LogDebug("Result for schedule check of {Name} {Result}",
                    schedule.Name,
                    checkResult);
            }

            _logger.LogDebug("Checked and executed Schedules {Result}", resultList);
            return resultList.TrueForAll(r => r.Success);
        }


        /// <summary>
        ///     Check if a task is in schedule to be started.
        /// </summary>
        /// <param name="scheduleType">Action task  to check</param>
        /// <returns></returns>
        private async Task<TaskResult> CheckForActionOfScheduleType(
            ScheduleTypeEnum scheduleType) {
            _logger.LogDebug("Get schedule for action task {Type}", scheduleType);
            var schedule = await _scheduleRepository.GetSchedule(scheduleType);
            if (schedule.Id == 0) {
                _logger.LogWarning("Schedule not found");
                return new TaskResult {
                    Success = false,
                    Message = "Invalid Schedule Type. No Schedule could be found."
                };
            }

            var lastAction = await _scheduleArduinoService.GetLatestGardenTask(schedule);
            var isRunning = await _scheduleArduinoService.CheckIfScheduleIsRunning(schedule);

            if (_scheduleStartService.CheckIfShouldBeStarted(schedule, lastAction, isRunning.Running))
                return await _scheduleArduinoService.ExecuteScheduleStartAction(schedule);

            if (_scheduleStopService.CheckIfShouldBeStopped(schedule, lastAction, isRunning.Running))
                return await _scheduleArduinoService.ExecuteScheduleStopAction(schedule);

            _logger.LogDebug("No open schedule action was needed for schedule type: {Type}", scheduleType);
            return new TaskResult {
                Success = true,
                Message = "No open schedule action was needed."
            };
        }
    }
}