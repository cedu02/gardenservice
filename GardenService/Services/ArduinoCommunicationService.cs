using System;
using System.Text.Json;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenService.Interfaces;
using GardenService.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace GardenService.Services {
    public class ArduinoCommunicationService : IArduinoCommunicationService {
        private const string StartMarker = "<";
        private const string StopMarker = ">";

        private readonly GrowArduino _growArduino;
        private readonly ILogger<ArduinoCommunicationService> _logger;
        private readonly ReservoirArduino _reservoirArduino;
        private readonly ISerialPortService _serialPortService;


        public ArduinoCommunicationService(IOptions<ReservoirArduino> reservoirArduino,
            IOptions<GrowArduino> growArduino,
            ISerialPortService serialPortService,
            ILogger<ArduinoCommunicationService> logger) {
            _growArduino = growArduino.Value;
            _reservoirArduino = reservoirArduino.Value;
            _logger = logger;
            _serialPortService = serialPortService;
        }

        /// <summary>
        ///     Send Task to Arduino.
        /// </summary>
        /// <param name="action"></param>
        /// <returns>String result from arduino</returns>
        public async Task<string> ExecuteTaskOnArduino(ArduinoTaskEnum action,
            ArduinoEnum arduino) {
            // define port to send. 
            var port = arduino == ArduinoEnum.GrowArduino ? _growArduino.Port : _reservoirArduino.Port;

            // send task to selected arudino
            var buffer = await _serialPortService.SendAndReceive(port, action.ActionCode,
                StartMarker, StopMarker);

            // Remove start and stop marker from response. 
            var result = buffer.Replace("<", string.Empty)
                .Replace(">", string.Empty);

            return result;
        }

        /// <summary>
        ///     Send Task to ardunio and get Object result back.
        /// </summary>
        /// <param name="action"></param>
        /// <param name="arduino"></param>
        /// <typeparam name="T">Expected Result object.</typeparam>
        /// <returns>T Object from arduino result.</returns>
        public async Task<T> ExecuteTaskOnArduinoResult<T>(ArduinoTaskEnum action,
            ArduinoEnum arduino) where T : new() {
            _logger.LogDebug("Send Task to Arduino");
            var result = await ExecuteTaskOnArduino(action, arduino);
            try {
                _logger.LogDebug("Try to Deserialize response");
                return JsonSerializer.Deserialize<T>(result);
            }
            catch (Exception e) {
                _logger.LogError(e,
                    "Failed to deserialize result form arduino for Task: {action}",
                    action);
                return new T();
            }
        }
    }
}