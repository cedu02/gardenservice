using GardenCore.Interfaces.Repositories;
using GardenDB.Database.Repositories;
using GardenDB.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace GardenService.Services.ServiceCollections {
    public static class RepositoryServiceCollection {
        public static IServiceCollection AddRepositories(this IServiceCollection services) {
            services.AddTransient<IScheduleRepository, ScheduleRepository>();
            services.AddTransient<IGardenTaskRepository, GardenTaskRepository>();
            services.AddTransient<IGardenTaskTypeRepository, GardenTaskTypeRepository>();
            services.AddTransient<IAirTempRepository, AirTempRepository>();
            services.AddTransient<IGasSensorRepository, GasSensorRepository>();
            services.AddTransient<IPhRepository, PhRepository>();
            services.AddTransient<IPlantHumidityRepository, PlantHumidityRepository>();
            services.AddTransient<IPressureRepository, PressureRepository>();
            services.AddTransient<IScheduleTypeRepository, ScheduleTypeRepository>();
            services.AddTransient<ITdsRepository, TdsRepository>();
            services.AddTransient<IUvRepository, UvRepository>();
            services.AddTransient<IWaterLevelRepository, WaterLevelRepository>();
            services.AddTransient<IWaterLevelTypeRepository, WaterLevelTypeRepository>();
            services.AddTransient<IWaterTemperatureRepository, WaterTemperatureRepository>();
            services.AddTransient<ISensorRepository, SensorRepository>();
            services.AddTransient<ISensorTypeRepository, SensorTypeRepository>();

            services.AddTransient<ISensorDataRepositoryFactory, SensorDataRepositoryFactory>();
            services.AddTransient<IDbService, DbService>();

            return services;
        }
    }
}