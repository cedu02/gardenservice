using GardenService.Interfaces;
using GardenService.Services.Schedule;
using Microsoft.Extensions.DependencyInjection;

namespace GardenService.Services.ServiceCollections {
    public static class GardenServiceCollection {
        public static IServiceCollection AddGardenServices(this IServiceCollection services) {
            // Serial Port 
            services.AddTransient<ISerialPortService, SerialPortService>();

            // Arduino 
            services.AddTransient<IArduinoCommunicationService, ArduinoCommunicationService>();
            services.AddTransient<IArduinoService, ArduinoService>();

            // Schedule 
            services.AddTransient<ScheduleService>();
            services.AddTransient<IScheduleArduinoService, ScheduleArduinoService>();
            services.AddTransient<IScheduleStartService, ScheduleStartService>();
            services.AddTransient<IScheduleStopService, ScheduleStopService>();

            // Collectors
            services.AddTransient<SensorDataCollector>();
            services.AddTransient<CacheTaskService>();
            services.AddTransient<DatabaseSyncService>();
            services.AddTransient<InitializationService>();
            services.AddTransient<SensorDataCollector>();

            return services;
        }
    }
}