using GardenCore.Interfaces.Services.Cache;
using GardenDB.Cache.Interfaces;
using GardenDB.Cache.Services;
using Microsoft.Extensions.DependencyInjection;

namespace GardenService.Services.ServiceCollections {
    public static class CacheServiceCollection {
        public static IServiceCollection AddCacheServices(this IServiceCollection services) {
            services.AddTransient<ICacheService, CacheService>();
            services.AddTransient<ISensorCacheService, SensorCacheService>();
            services.AddTransient<ITaskCollector, TaskCollector>();

            return services;
        }
    }
}