using System;
using System.IO.Ports;
using System.Threading.Tasks;
using GardenService.Interfaces;
using Microsoft.Extensions.Logging;

namespace GardenService.Services {
    public class SerialPortService : ISerialPortService {
        private readonly ILogger<SerialPortService> _logger;

        private readonly SerialPort _serialPort;

        public SerialPortService(ILogger<SerialPortService> logger) {
            _logger = logger;
            _serialPort = new SerialPort();
        }

        public async Task<string> SendAndReceive(string port,
            int taskNumber, string startMarker, string stopMarker) {
            _serialPort.PortName = port;
            _serialPort.BaudRate = 9600;
            _serialPort.Parity = Parity.None;
            _serialPort.StopBits = StopBits.One;
            _serialPort.DataBits = 8;
            _serialPort.Handshake = Handshake.None;
            _serialPort.ReadTimeout = 500;
            _serialPort.WriteTimeout = 500;

            try {
                _serialPort.Open();
            }
            catch (Exception e) {
                _logger.LogError(e, "Failed to open com port {port}", port);
                return startMarker + "{Success: false, Message: 'Failed to open com port'}" + stopMarker;
            }

            string result;
            try {
                var readTask = new Task<string>(() => DataReceived(taskNumber, startMarker, stopMarker));
                readTask.Start();
                await readTask;
                result = readTask.Result;
            }
            catch (Exception e) {
                _logger.LogError(e, "Failed to get result from com port {port}", port);
                return startMarker + "{\"Success\": false, \"Message\": \"Failed to read result from com port\"}" +
                       stopMarker;
                ;
            }

            try {
                _serialPort.Close();
            }
            catch (Exception e) {
                _logger.LogError(e, "Failed to close com port {port}", port);
            }

            return result;
        }

        public bool IsAvailable(string port) {
            var serialPort = new SerialPort {
                PortName = port,
                BaudRate = 9600,
                Parity = Parity.None,
                StopBits = StopBits.One,
                DataBits = 8,
                Handshake = Handshake.None,
                ReadTimeout = 500,
                WriteTimeout = 500
            };

            try {
                serialPort.Open();
                return true;
            }
            catch {
                return false;
            }
            finally {
                serialPort.Close();
            }
        }

        private string DataReceived(int action, string startMarker, string stopMarker) {
            _logger.LogInformation("Send action to com port {action}", action);

            var reading = true;
            var buffer = string.Empty;
            try {
                _serialPort.WriteLine(startMarker + action + stopMarker);
            }
            catch (Exception e) {
                _logger.LogError(e, "Failed to send action to com port {action}", action);
                throw;
            }


            var start = DateTime.Now;
            while (reading || (DateTime.Now - start).Seconds < 20) {
                _logger.LogDebug("Before serial port readline is executed");
                try {
                    buffer += _serialPort.ReadLine();
                }
                catch {
                    _serialPort.WriteLine(startMarker + action + stopMarker);
                }

                _logger.LogDebug("Serial Port response {value}", buffer);
                if (buffer != string.Empty && buffer.Substring(0, 1) != startMarker) buffer = string.Empty;

                if (buffer.Contains(stopMarker)) reading = false;
            }

            return buffer ?? string.Empty;
        }
    }
}