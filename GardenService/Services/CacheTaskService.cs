using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Interfaces.Repositories;
using GardenCore.Models;
using GardenDB.Cache.Interfaces;
using GardenService.Interfaces;
using GardenService.Models;
using Microsoft.Extensions.Logging;

namespace GardenService.Services {
    public class CacheTaskService {
        private readonly IArduinoService _arduinoService;

        private readonly IGardenTaskRepository _gardenTaskRepository;
        private readonly ILogger<CacheTaskService> _logger;
        private readonly ITaskCollector _taskCollector;

        public CacheTaskService(ITaskCollector taskCollector,
            ILogger<CacheTaskService> logger,
            IGardenTaskRepository gardenTaskRepository,
            IArduinoService arduinoService) {
            
            _taskCollector = taskCollector;
            _logger = logger;
            _arduinoService = arduinoService;
            _gardenTaskRepository = gardenTaskRepository;
        }

        /// <summary>
        ///     Check cache for action.
        /// </summary>
        /// <returns></returns>
        public async Task<bool> CheckForOpenTasksInCache() {
            var switchTasks = GardenTaskEnum.GetAllSwitchTasks();
            var resultList = new List<SwitchResults>();

            foreach (var switchTask in switchTasks) {
                _logger.LogDebug("Check for {Name} switch task", switchTask.Name);
                var checkResult = await CheckTask(switchTask);
                resultList.Add(new SwitchResults {
                    Success = checkResult.Success,
                    SwitchTask = switchTask
                });
                _logger.LogInformation("Result for {Name} switch task test: {Result}",
                    switchTask.Name,
                    checkResult);
            }

            return resultList.TrueForAll(r => r.Success);
        }


        /// <summary>
        ///     Check for switch task in redis cache.
        ///     When one is found send appropriate action to arduino to execute.
        /// </summary>
        /// <param name="gardenTask"></param>
        /// <returns></returns>
        private async Task<TaskResult> CheckTask(GardenTaskEnum gardenTask) {
            var task = await ReadSwitchTask(gardenTask);
            if (!task.TaskFound)
                return new TaskResult {
                    Success = true,
                    Message = "No task found"
                };

            var action = task.StatusToChangeTo
                ? gardenTask.ScheduleType.StartAction
                : gardenTask.ScheduleType.StopAction;

            var taskExecutionResult = await ExecuteTaskOnArduino(action, gardenTask);
            if (taskExecutionResult) {
                await SaveActionToDatabase(action);
                await _taskCollector.SetNewState(gardenTask, task.StatusToChangeTo);
            }

            return new TaskResult {
                Success = taskExecutionResult,
                Message = taskExecutionResult ? "Successful" : "Failed to execute Task"
            };
        }

        /// <summary>
        ///     Send task to arduino for execution.
        /// </summary>
        /// <param name="action"></param>
        /// <param name="gardenTask"></param>
        /// <returns></returns>
        private async Task<bool> ExecuteTaskOnArduino(ArduinoTaskEnum action, GardenTaskEnum gardenTask) {
            _logger.LogDebug("Switch task found. Action to execute: {Action}", action);
            var result = await _arduinoService.ExecuteTask<TaskResult>(
                action,
                gardenTask.Arduino);

            if (!result.Success) _logger.LogWarning("Failed to execute switch task on arduino. {Result}", result);

            return result.Success;
        }


        /// <summary>
        ///     Read form cache if there is a open switch task.
        /// </summary>
        /// <param name="gardenTask"></param>
        /// <returns></returns>
        private async Task<TaskModel> ReadSwitchTask(GardenTaskEnum gardenTask) {
            _logger.LogDebug("Check if there is a switch task for: {Key}", gardenTask);
            var task = await _taskCollector.GetSwitchTask(gardenTask);
            if (!task.TaskFound) _logger.LogDebug("No open switch task found");

            return task;
        }

        /// <summary>
        ///     Log action execution to database for later analysing.
        /// </summary>
        /// <param name="arduinoTask"></param>
        /// <returns></returns>
        private async Task SaveActionToDatabase(ArduinoTaskEnum arduinoTask) {
            var executedAction = new GardenTaskModel {
                ArduinoTask = arduinoTask,
                ScheduleId = 0,
                Timestamp = DateTime.Now
            };

            _logger.LogDebug("Log executed action in database {Action}", executedAction);
            var actionCreated = await _gardenTaskRepository.SaveAction(executedAction);

            if (!actionCreated.Success) _logger.LogWarning("Action could not be saved {Result}", actionCreated);
        }
    }
}