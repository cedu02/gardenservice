using System.Threading.Tasks;
using GardenCore.Enums;
using GardenService.Interfaces;
using Microsoft.Extensions.Logging;

namespace GardenService.Services {
    public class ArduinoService : IArduinoService {
        private readonly IArduinoCommunicationService _acs;
        private readonly ILogger<ArduinoService> _logger;

        public ArduinoService(IArduinoCommunicationService acs,
            ILogger<ArduinoService> logger) {
            _acs = acs;
            _logger = logger;
        }

        public async Task<T> ExecuteTask<T>(ArduinoTaskEnum action, ArduinoEnum arduino) where T : new() {
            _logger.LogDebug("Execute {Action} Task", action.Value);
            var result = await _acs.ExecuteTaskOnArduinoResult<T>(action, arduino);
            _logger.LogDebug("Executed Task with result: {Result}", result);
            return result;
        }
    }
}