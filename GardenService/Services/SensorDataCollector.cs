using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GardenCore.Enums;
using GardenCore.Interfaces.Services.Cache;
using GardenCore.Models;
using GardenService.Interfaces;
using Microsoft.Extensions.Logging;

namespace GardenService.Services {
    public class SensorDataCollector {
        private readonly IArduinoService _arduinoService;
        private readonly ILogger<SensorDataCollector> _logger;
        private readonly ISensorCacheService _sensorCacheService;

        private readonly List<SensorEnum> _sensors = SensorEnum.GetAllSensors();

        public SensorDataCollector(ISensorCacheService sensorCacheService,
            IArduinoService arduinoService,
            ILogger<SensorDataCollector> logger) {
            _sensorCacheService = sensorCacheService;
            _logger = logger;
            _arduinoService = arduinoService;
        }

        /// <summary>
        ///     Read sensor data and save to cache.
        /// </summary>
        /// <returns></returns>
        public async Task<TaskResult> CollectSensorData() {
            var collectionResult = new TaskResult {
                Success = true
            };

            foreach (var sensor in _sensors) {
                dynamic sensorData;
                try {
                    _logger.LogDebug("Get data from arduino for Sensor Task {sensorTask}", sensor);
                    var method = _arduinoService.GetType().GetMethod(nameof(_arduinoService.ExecuteTask));
                    var generic = method?.MakeGenericMethod(sensor.DataTask.DataType);
                    if (generic == null) {
                        _logger.LogError("Failed to construct arduino service for datatype {type}",
                            sensor.DataTask.DataType);
                        collectionResult.Success = false;
                        continue;
                    }

                    dynamic result = generic.Invoke(_arduinoService,
                        new object[] {sensor.DataTask, sensor.DataTask.Arduino})!;
                    sensorData = await result;

                    _logger.LogDebug("Response form arduino for sensor task: {sensorTask}", sensor);
                }
                catch (Exception ex) {
                    _logger.LogError(ex, "Arduino failed to read sensor data for sensor {sensor}", sensor);
                    collectionResult.Success = false;
                    continue;
                }

                try {
                    _logger.LogDebug("Save data to caches}", sensor);
                    var saveResult = await _sensorCacheService.SetValue(sensor, sensorData);

                    if (!saveResult) {
                        _logger.LogDebug("Failed to save data to cache for sensor {s}", sensor);
                        collectionResult.Success = false;
                        continue;
                    }

                    _logger.LogDebug("Saved data to cache for sensor {sensor}", sensor);
                }
                catch (Exception ex) {
                    _logger.LogError(ex, "Arduino returned unexpected data type for sensor {sensor}", sensor);
                    collectionResult.Success = false;
                }
            }

            return collectionResult;
        }
    }
}