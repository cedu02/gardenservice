using System;
using GardenDB.Database.Models;
using GardenService.Models;
using GardenService.Services.ServiceCollections;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace GardenService {
    public class Program {
        public static void Main(string[] args) {
            var host = CreateHostBuilder(args).Build();
            CreateDbIfNotExists(host);
            host.Run();
        }

        private static void CreateDbIfNotExists(IHost host) {
            var scope = host.Services.CreateScope();
            var services = scope.ServiceProvider;

            try {
                var context = services.GetRequiredService<GardenDbContext>();
                context.Database.EnsureCreated();
            }
            catch (Exception ex) {
                var logger = services.GetRequiredService<ILogger<Program>>();
                logger.LogError(ex, "DB creation failed");
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) {
            return Host.CreateDefaultBuilder(args)
                .UseSystemd()
                .ConfigureServices((hostContext, services) => {
                    services.AddHostedService<Worker>();

                    var redisConfig = hostContext.Configuration
                        .GetSection("Redis")
                        .GetSection("URL")
                        .Value ?? string.Empty;
                    
                    services.AddStackExchangeRedisCache(options => {
                        options.Configuration = redisConfig;
                        options.InstanceName = "GardenInstance";
                    });

                    services.Configure<ReservoirArduino>(hostContext.Configuration
                        .GetSection("ReservoirArduino"));

                    services.Configure<GrowArduino>(hostContext.Configuration
                        .GetSection("GrowArduino"));

                    services.AddDbContext<GardenDbContext>(options => options
                        .UseSqlite("Data Source=garden.db"));

                    services.AddGardenServices();
                    services.AddCacheServices();
                    services.AddRepositories();
                });
        }
    }
}